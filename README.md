## Hello to Express and Pug

In this project, the aim was understanding the basics of Express and Pug
in NodeJS platform.

### How to run the project ?
To run the project, you need to have nodejs and npm installed in your system.
  
There is only 1 Javascript file is included in the project as server1.js, to
execute that file first dependencies are needed to be installed. To install them
use  "npm install".  
Following run "node server1.js" and head to the localhost:7000