/** This is a tutorial project that was followed 
 * from https://freshman.tech/learn-node/
 * 
 * @author canberkardic
 */

const express = require("express");
const app = express();

app.set('view engine', 'pug')
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.render('index', {
        name: 'sarah',
        title: 'Express and Pug'
    });
});

app.get('/hey', (req, res) => {
    res.render('hello', {
        name: 'Canberk',
        title: 'Hello Page'
    });
});


const server = app.listen(7000, () => {
    console.log(`Express running → PORT ${server.address().port}`);
});

